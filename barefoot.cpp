/*
Barefoot by Klaatu (notklaatu@hackerpublicradioNNNospanmNNNorg)
Convert .fountain screenplay files to plain text from a unix shell

REQUIRES
--------

- c++
- boost

LICENSE
-------

GPL v3
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <boost/regex.hpp>

using namespace std;


int numero( string enigo ) {
  ifstream enrivereto;
  string rango;
  int wc = 0;
  enrivereto.open( enigo.c_str() );

  while (std::getline(enrivereto, rango)) {
    wc++; }
  enrivereto.close();

  return wc;
}


void teksto(string frazo, int krommargxeno, int pagxo) {
  int sigLongo = frazo.size();
  string vorto;
  string linio;

  if ( sigLongo > pagxo ) {

    for(int nombrilo=0; sigLongo + 1 > nombrilo; nombrilo++) {

      if ( nombrilo >= sigLongo ) {
	linio += vorto;
	cout << string( krommargxeno, ' ' );
	cout << linio << endl;
       	break;
      } 

      else if(frazo[nombrilo] != ' ') {
	vorto += frazo[nombrilo];
      }

      else {

	if ((vorto.size()) + (linio.size()) >= pagxo) {
	  cout << string( krommargxeno, ' ' );
	  cout << linio << endl;
	  linio.clear();
	}
	linio += vorto;
	linio += ' ';
	vorto.clear();
	}//else
    } //FOR frazo-nombrilo
  } //if sigLongo

  else {
    cout << string( krommargxeno, ' ' );
    cout << setw(pagxo) << frazo << endl;
  } //else

} //teksto


int main( int argc, char *argv[] ) {
  if(argc > 1) {
  string enigo = argv[1];
  ifstream enrivereto;
  string frazo;
  string kategorio;
  int wc;
  int pagxo = 80;
  int alineo = 40;
  int charhom = 26;
  int charwry = 19;
  int charpar = 18;

  boost::regex okazejo( "^[E,I][N,X]T\\.[[:space:]].*[[:space:]]-[[:space:]].*" );
  boost::regex homo( "[A-Z ]+\\(?[A-Z [:punct:]\\.']*\\)?$" );
  boost::regex transiro( "^CUT.*|^MATCH CUT.*|.*FADE .*|.*TITLE.*|.* TO.*|^DISSOLVE.*|^TRANSITION.*:" );
  boost::regex emocio( "^[A-Z ]+\\." );
  boost::regex stilo( "^>.+" );
  boost::regex wryly( "^\\(.*\\)$" );

  enrivereto.open( enigo.c_str() );

  wc = numero(enigo);
  // debug cout << wc << "\n";
  int n = 0;
  int paroloj = 0;
  string sitelo;

  while ( n < wc ) {
    getline(enrivereto, frazo);

    if ( n > 0 ) { // frazo ekzistas
      boost::smatch egale;
      if ( frazo.empty() ) {
	cout << "\n" << endl;
	// ili estas ne parolis.
        paroloj = 0;
      }
      else if ( paroloj > 0 ) {
	// ĉi tio estas parolo
	if (boost::regex_match(frazo, egale, wryly)) {
	  teksto(frazo,charwry,alineo);
	}
	else {
	  teksto(frazo,charpar,alineo);
	}

      }
      else if (boost::regex_match(frazo, egale, okazejo)) {
	// okazejo
	cout << left;
	cout << setw(pagxo) << frazo << endl;
      }
      else if (boost::regex_match(frazo, egale, homo)) {
	if (boost::regex_match(frazo, egale, transiro)) {
	  // transiro
	  cout << right;
	  cout << setw(pagxo) << frazo << endl;
	  // ili estas ne parolis.
	  paroloj = 0;
	}
	else if (boost::regex_match(frazo, egale, emocio)) {
	  // instrukcio, eleganta
	  cout << left;
	  cout << std::setw(alineo) << frazo << endl;
	}
	else {
	  // homoj
	  paroloj = 1;
	  cout << string( charhom, ' ' );
	  cout << left;
	  cout << std::setw(alineo) << frazo << endl;
	}
      }
      else if (boost::regex_match(frazo, egale, stilo)) {
	// eleganta teksto ( >_ **BLAH )
	cout << left;
	teksto(frazo,charpar,pagxo);
	}
      else {
	// instrukcioj
	teksto(frazo,0,pagxo);
      }
      

    } //n>0

    n++;

  } //while

  enrivereto.close();

  return 0;
}
  else {
    cout << "Provide a .fountain file to process." << endl;
    return 1;
  }
}
