Barefoot
==========

Convert ``.fountain`` screenplay files to formatted, plain text screenplays, ready for printing to paper or PDF, all from a Unix shell.

Brought to you by `Slackermedia <http://slackermedia.info>`_, with a nice pine-scented open source license (GPL3), so feel free to share or contribute.

For information on Fountain by John August, Nima Yousefi, and Stu Maschwitz, go to `fountain.io <http://fountain.io>`_


Usage
-------

This is, like any good shell utility, a modular piece of a larger pipeline, so you will want to use it along with the ``pr`` command and either ``lpr`` or `text2pdf <http://www.eprg.org/pdfcorner/text2pdf/>`_

The workflow is basically something like this to print:

    $ barefoot myScreenplay.fountain | pr -f -t | lpr


Or this to print-to-PDF:

    $ barefoot myScreenplay.fountain | pr -f -t | text2pdf > myScreenplay.txt 


Status
-------

[27 June 2016]
I've been using this on fountain files for the past year and it's been doing well. Always interested to hear edge cases or bug reports.

[16 June 2015]
This program is in beta. A little more testing and polishing, and then a release. If you are a technical user, you should be able to use this, but you will have to compile it yourself, and it's still rough around the edges. 

[14 June 2015]
This program is pre-alpha, meaning you should not attempt to use it unless you are a developer.


System Requirements
--------------------

To build and use this application, you must have:

   * GCC (specifically, g++)
   * C++ (probably already on your system)
   * Boost (headers and regex libs)


Note:
-------

Linux or Unix (BSD, Illumos) are supported. 

It should work fine on everything else, but I am unable to test, as I do not run closed source operating systems personally and haven't got access to any closed source systems (certainly not to ones with all the dev tools needed to compile).