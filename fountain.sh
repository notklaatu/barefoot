#!/bin/bash
ARG="${1}"

cname() {
    CHARACTER=`echo "${1}" | sed -n '/^[A-Z ].[A-Z ]*[(A-Z )|)]$/p'`
    if [ x"$CHARACTER" == "x" ]; then
	false
    else
	echo "this a character". $CHARACTER
    fi
}

slug() {
    SLUG=`echo $1 | sed -n '/^[E,I][N,X]T. .* - [D|N|C]/p'`
    echo $SLUG
}

N=1
M=2
C=$(wc -l "${ARG}" | cut -f1 -d' ' )

while [ $N -lt $C ]
do
    NUN=`sed -n "${N}p" "${ARG}"`
    PROKSIMA=`sed -n "${M}p" "${ARG}"`
    #echo "NUN" $NUN
    #echo "PROKSIMO" $PROKSIMA
    cname "${NUN}" "${PROKSIMA}"
    ((N++))
    ((M++))
done


#do
#    cname "${line}"
#done < "${ARG}"

#SLUG
#

#voice direction
#sed -n '/^(.*)\s*$/p' fish.fountain

#Screen direction
#/^\s*\n[A-Z][A-Za-z0-9 \-].*$\n\s/

#transition
#^([A-Z].*[A-Z|)](:)$)
